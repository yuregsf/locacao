package controller;

import model.Alugar;
import model.DAO.AlugarDAO;
import model.DAO.EquipamentoDAO;
import model.DAO.ReservaDAO;
import model.DAO.UsuarioDAO;
import model.Equipamento;
import model.Usuario;
import model.Reserva;

public class Controller {

    public boolean addUsuario(String nome, String telefone, String senha, String email){
        Usuario u = new Usuario();
        u.setNome(nome);
        u.setTelefone(telefone);
        u.setIs_admin(false);
        u.setIs_cliente(true);
        u.setSenha(senha);
        u.setEmail(email);
        new UsuarioDAO().save(u);
        return true;
    }
    
    public boolean addEquipamento(String nome, String desc){
        Equipamento e = new Equipamento();
        e.setNome(nome);
        e.setDesc(desc);
        new EquipamentoDAO().save(e);
        return true;
    }
    
    public boolean addReserva(String dt_reserva, String dt_devolucao, Equipamento equipamento, Usuario usuario){
        Reserva r = new Reserva();
        r.setDt_devolucao(dt_devolucao);
        r.setDt_reserva(dt_reserva);
        r.setEquipamento(equipamento);
        r.setUsuario(usuario);
        new ReservaDAO().save(r);
        return true;
    }
    
    public boolean addAlugar(String dt_alugar, String dt_devolucao, Equipamento equipamento, Usuario usuario){
        Alugar a = new Alugar();
        a.setDt_alugar(dt_alugar);
        a.setDt_devolucao(dt_devolucao);
        a.setEquipamento(equipamento);
        a.setUsuario(usuario);
        new AlugarDAO().save(a);
        return true;
    }
}
