package controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author joao-filho
 */
public class ConnectionFactory {

    public  EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");

    /**
     * * Método responsável por retornar um EntityManager **
     */
    public EntityManager getConnection() {
        return Persistence.createEntityManagerFactory("PU").createEntityManager();
    }
}
