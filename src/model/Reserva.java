package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity(name = "reserva")
public class Reserva implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_reserva;
    private String dt_reserva;
    private String dt_devolucao;
    @ManyToOne
    @JoinColumn(name = "id_equipamento")
    private Equipamento equipamento;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    

    public int getId_reserva() {
        return id_reserva;
    }

    public void setId_reserva(int id_reserva) {
        this.id_reserva = id_reserva;
    }

    public String getDt_reserva() {
        return dt_reserva;
    }

    public void setDt_reserva(String dt_reserva) {
        this.dt_reserva = dt_reserva;
    }

    public String getDt_devolucao() {
        return dt_devolucao;
    }

    public void setDt_devolucao(String dt_devolucao) {
        this.dt_devolucao = dt_devolucao;
    }

    
    
    
    
}
