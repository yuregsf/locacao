package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "alugar")
public class Alugar implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_alugar;
    private String dt_alugar;
    private String dt_devolucao;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    @ManyToOne
    @JoinColumn(name = "id_equipamento")
    private Equipamento equipamento;    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public int getId_alugar() {
        return id_alugar;
    }

    public void setId_alugar(int id_alugar) {
        this.id_alugar = id_alugar;
    }

    public String getDt_alugar() {
        return dt_alugar;
    }

    public void setDt_alugar(String dt_alugar) {
        this.dt_alugar = dt_alugar;
    }

    public String getDt_devolucao() {
        return dt_devolucao;
    }

    public void setDt_devolucao(String dt_devolucao) {
        this.dt_devolucao = dt_devolucao;
    }    
    
    
}
