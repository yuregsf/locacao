
package model.DAO;

import controller.ConnectionFactory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import model.Alugar;

public class AlugarDAO {
    
    public void save(Alugar a) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            if (a.getId_alugar() == 0) {
                emManager.persist(a);
            } else {
                emManager.merge(a);
            }
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
    }
      public void remove(Alugar p) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            Alugar aux = emManager.merge(p);
            emManager.remove(aux);
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
    }
       public Alugar get(int codPes) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        Alugar p = new Alugar();

        try {
            p = emManager.find(Alugar.class, codPes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return p;
    }
       public List<Alugar> Listar() {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Alugar> lPes = new ArrayList<Alugar>();

        try {
            Query q = emManager.createQuery("SELECT p FROM Alugar p");
            lPes = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return lPes;
    }
           public List<Alugar> search(String query) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Alugar> lAlug = new ArrayList<Alugar>();

        try {
            Query q = emManager.createQuery("SELECT p FROM AlugAr p WHERE p.nome LIKE :nome");
            q.setParameter("nome", "%" + query + "%");
            lAlug = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return lAlug;
    }
}
