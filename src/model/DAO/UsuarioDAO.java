package model.DAO;

import controller.ConnectionFactory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import model.Usuario;

/**
 *
 * @author Yure Gabriel
 */
public class UsuarioDAO {

    /**
     * * Método responsável por adicionar ou atualizar um registro no banco de
     * dados **
     */
    public void save(Usuario a) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            if (a.getId_usuario()== 0) {
                emManager.persist(a);
            } else {
                emManager.merge(a);
            }
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
    }

    /**
     * * Método responsável por deletar um registro no banco de dados **
     */
    public void remove(Usuario a) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            Usuario aux = emManager.merge(a);
            emManager.remove(aux);
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
    }

    /**
     * * Método responsável por obter um registro no banco de dados **
     */
    public Usuario get(int id_usuario) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        Usuario a = new Usuario();

        try {
            a = emManager.find(Usuario.class, id_usuario);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return a;
    }

    /**
     * * Método responsável por obter todos os registros no banco de dados **
     */
    public static List<Usuario> Listar() {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Usuario> lUsuario = new ArrayList<Usuario>();

        try {
            Query q = emManager.createQuery("SELECT a FROM usuario a");
            lUsuario = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return lUsuario;
    }

    /**
     * * Método responsável por pesquisar registros no banco de dados **
     */
    public List<Usuario> search(String query) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Usuario> lUsuario = new ArrayList<Usuario>();

        try {
            Query q = emManager.createQuery("SELECT a FROM usuario a WHERE a.nome LIKE :nome");
            q.setParameter("nome", "%" + query + "%");
            lUsuario = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return lUsuario;
    }

    public boolean login(String email, String senha) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Usuario> lUsuario = new ArrayList<Usuario>();

        try {
            Query q = emManager.createQuery("SELECT a FROM usuario a WHERE a.email LIKE :email AND a.senha LIKE :senha AND is_admin = 1");
            q.setParameter("email", email);
            q.setParameter("senha", senha);
            lUsuario = q.getResultList();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        if (!lUsuario.isEmpty()){            
            return true;            
        }else{
            return false;            
        }
    }
}
