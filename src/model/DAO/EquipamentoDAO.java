
package model.DAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import model.Equipamento;
import controller.ConnectionFactory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author aluno
 */
public class EquipamentoDAO {
    public void save(Equipamento a) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            if (a.getId_equipamento() == 0) {
                emManager.persist(a);
            } else {
                emManager.merge(a);
            }
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
            etTransaction.rollback();
        } finally {
            emManager.close();
        }
    }
    public void remove(Equipamento a) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            Equipamento aux = emManager.merge(a);
            emManager.remove(aux);
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
    }
    public Equipamento get(int codAni) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        Equipamento a = new Equipamento();

        try {
            a = emManager.find(Equipamento.class, codAni);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return a;
    }
    public static List<Equipamento> Listar() {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Equipamento> lequip = new ArrayList<Equipamento>();

        try {
            Query q = emManager.createQuery("SELECT a FROM equipamento a");
            lequip = q.getResultList();
              return lequip;
        } catch (Exception e) {
            e.printStackTrace();
                 return new ArrayList<>();
        } finally {
            emManager.close();
        }
      
    }
        public List<Equipamento> search(String query) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Equipamento> lequip = new ArrayList<Equipamento>();

        try {
            Query q = emManager.createQuery("SELECT a FROM Equipamento a WHERE a.nome LIKE :nome");
            q.setParameter("nome", "%" + query + "%");
            lequip = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return lequip;
    }

}
