package model.DAO;

import controller.ConnectionFactory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import model.Equipamento;
import model.Reserva;

/**
 *
 * @author Yure Gabriel
 */
public class ReservaDAO {

    /**
     * * Método responsável por adicionar ou atualizar um registro no banco de
     * dados **
     */
    public void save(Reserva a) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            if (a.getId_reserva()== 0) {
                emManager.persist(a);
            } else {
                emManager.merge(a);
            }
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
    }

    /**
     * * Método responsável por deletar um registro no banco de dados **
     */
    public void remove(Reserva a) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        EntityTransaction etTransaction = emManager.getTransaction();

        try {
            etTransaction.begin();
            Reserva aux = emManager.merge(a);
            emManager.remove(aux);
            etTransaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
    }

    /**
     * * Método responsável por obter um registro no banco de dados **
     */
    public Reserva get(int id_reserva) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        Reserva a = new Reserva();

        try {
            a = emManager.find(Reserva.class, id_reserva);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return a;
    }

    /**
     * * Método responsável por obter todos os registros no banco de dados **
     */
    public List<Reserva> Listar() {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Reserva> lReserva = new ArrayList<Reserva>();

        try {
            Query q = emManager.createQuery("SELECT a FROM reserva a");
            lReserva = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return lReserva;
    }
    

    /**
     * * Método responsável por pesquisar registros no banco de dados **
     */
    public List<Reserva> findByEquipamento(Equipamento query) {
        EntityManager emManager = new ConnectionFactory().getConnection();
        List<Reserva> lReserva = new ArrayList<Reserva>();

        try {
            Query q = emManager.createQuery("SELECT a FROM reserva a WHERE a. LIKE :nome");
            q.setParameter("nome", "%" + query + "%");
            lReserva = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            emManager.close();
        }
        return lReserva;
    }

}
